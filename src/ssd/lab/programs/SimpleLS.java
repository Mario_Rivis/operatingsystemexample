package ssd.lab.programs;

import ssd.lab.core.File;
import ssd.lab.core.Program;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static ssd.lab.core.File.FilePermission.READ;

public class SimpleLS implements Program {

    private static Logger LOG = Logger.getLogger("SimpleLSLogger");
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MMM/yyyy");

    @Override
    public void run(String[] args) {
        if (args.length > 1) {
            LOG.warning("SimpleLS can get maximum one argument!");
            return;
        }

        System.out.println(String.format("Perm     Updated     Type    Size     Name     ENC"));
        System.out.println("======================================");

        List<File> allFiles = getFiles();

        int totalSize = 0;

        for (File file : allFiles) {
            if (file.getFilePermission().equals(READ))
                System.out.println("--r--");

            System.out.println("     ");

            System.out.println(simpleDateFormat.format(file.getLastUpdated()));

            System.out.println("     ");

            String fileType = "NONE";
            switch (file.getType()) {
                case FILE:
                    fileType = "FILE";
                    break;
                case ROOT:
                    fileType = "ROOT";
                    break;
                case FOLDER:
                    fileType = "FOLDER";
                    break;
            }

            System.out.println(fileType);

            System.out.println("      ");

            System.out.println(file.getContent().getText().getBytes().length);
            totalSize += file.getContent().getText().length();

            System.out.println(String.format("     %s", normalizePath(file.getAbsolutePath())));

            System.out.println(file.getContent().getCharset().aliases().contains("ASCII") ? "ASCII" : "OTHER");
        }


        System.out.println(String.format("The size of your directory is %d.", totalSize));
        System.out.println("====================================");
        System.out.println("Have a nice one!");

    }

    @Override
    public void connect() {

    }


    private String normalizePath(String absolutePath) {
        String reltaivePath = "";
        //get relativePath...
        return reltaivePath;
    }

    private List<File> getFiles() {
        List<File> returnList = new ArrayList<>();
        //get files for the specified folder...
        return returnList;
    }
}
