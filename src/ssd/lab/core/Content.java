package ssd.lab.core;

import java.nio.charset.Charset;

public class Content {

    private Charset charset;
    private String text;

    public Content(Charset charset, String text) {
        this.charset = charset;
        this.text = text;
    }

    public Charset getCharset() {
        return charset;
    }

    public void setCharset(Charset charset) {
        this.charset = charset;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
