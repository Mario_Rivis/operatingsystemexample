package ssd.lab.core;

import java.util.Date;

public class File {

    public enum FileType { FILE, FOLDER, ROOT }
    public enum FilePermission { READ }

    private FileType type;
    private String absolutePath;
    private Content content;
    private FilePermission filePermission;
    private Date lastUpdated;

    public File() {
    }

    public File(FileType type) {
        this.type = type;
    }

    public File(String absolutePath) {
        this.absolutePath = absolutePath;
    }

    public File(String absolutePath, Content content) {
        this.absolutePath = absolutePath;
        this.content = content;
    }

    public File(FileType type, String absolutePath, Content content, FilePermission filePermission) {
        this.type = type;
        this.absolutePath = absolutePath;
        this.content = content;
        this.filePermission = filePermission;
    }

    public FileType getType() {
        return type;
    }

    public void setType(FileType type) {
        this.type = type;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public FilePermission getFilePermission() {
        return filePermission;
    }

    public void setFilePermission(FilePermission filePermission) {
        this.filePermission = filePermission;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        File file = (File) o;

        return absolutePath.equals(file.absolutePath);
    }

    @Override
    public int hashCode() {
        return absolutePath.hashCode();
    }
}
