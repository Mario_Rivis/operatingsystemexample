package ssd.lab.core;

public interface Program {

    void run(String[] args);
    void connect();
}
